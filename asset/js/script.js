/*Create Array Literal 
	-use an array literal - array literal

*/
const array1 = ['eat', 'sleep'];
console.log(array1);

const array2 = new Array ('pray', 'play');
console.log(array2)


console.log('==========types of array==========');

const myList = []; //empty array

const numArray = [2, 3, 4, 5];  //array of numbers

 const stringArray = ['eat', 'work', ' pray', 'play'] //array of strings

 /*
 	Mini-Activity
	
	Create an array with 7 items; all strings.
		-List seven of the places you want to visit someday

		Log the first item in the console.
		Log all the items in the console.
 */

let placesWantToVisit = ['Japan', 'Batanes', 'Palawan', 'Thailand', 'Taiwan', 'Singapore', 'London'];
 console.log(placesWantToVisit [0]);
 console.log(placesWantToVisit);
 console.log(placesWantToVisit [placesWantToVisit.length-1]);



// for (let i = 0; i < placesWantToVisit.length; i++) {
// 	console.log(placesWantToVisit[i]);
// }

// Array Manipulation
// Add element to an array - push() add element at the end of the array

let dailyActivities = ['eat', 'work', 'pray', 'play'];
dailyActivities.push('exercise');
console.log(dailyActivities);
// unshift () add element at the beginning of the array

dailyActivities.unshift('sleep');
console.log(dailyActivities);

//add element at the middle an array

dailyActivities[2] = 'sing';
console.log(dailyActivities);

dailyActivities[5] = 'dance';
console.log(dailyActivities);

placesWantToVisit[3] = 'Armenia';
console.log(placesWantToVisit);

/* 
	re-assign the values for the first and last item in the array
	-Re-assign it with your hometown and your highschool
	Log the array in the console
	Log the first and Last items in the console.

*/

placesWantToVisit [0] = 'Davao City';
console.log(placesWantToVisit);

placesWantToVisit [6] = 'Holy Cross of Mintal';
console.log(placesWantToVisit);

console.log(placesWantToVisit[0]);
console.log(placesWantToVisit[6]);

// Adding items in an array without using methods:

let array = [];
console.log(array[0]);
array[0] = 'Cloud Strife';
console.log(array);
console.log(array[1]);
array[1] = 'Tifa Lockhart';
console.log(array[1]);
array[array.length-1] = 'Aerith Gainsborough';
console.log(array);
array[array.length] = 'Vicent Valentine';
console.log(array);

// Array Methods
	// Manipulate array with pre-determined JS Functions
	// Mutators - these arrays methods usually change the original array


	let array3 = ['Juan', 'Pedro', 'Jose', 'Andres'];
	
	// Without method
	array3[array.length] = 'Francisco';
	console.log(array3);

	// .push()-allows us to add an element at the end of he array

	array3.push('Andres');
	console.log(array3);

	// unshift() - allows us to add an element at the beginning of the array

	array3.unshift('Simon');
	console.log(array3);

	//.pop() - allows us to delete or remove the last item/element at the end of the array

	array3.pop();
	console.log(array3); 

	//.pop() is also able to return the item we removed

	console.log(array3.pop());
	console.log(array3);

	let removedItem = array3.pop();
	console.log(array3);
	console.log(removedItem);

	// .shift() return the item we removed

	let removedItemShift = array3.shift();
	console.log(array3);
	console.log(removedItemShift);

	/*
		Mini - Activity:

		Remove the first item of array3 and log array3 in the console.

		Delete the last item of array3 and Log array3 in the console.

		Add "George" at the start of the array3 and log array3 in the console.

		Add "Michael" at the end of the array1 and Log array3 in the console.

		Use array methods.

	*/

	let removedItemShift1 = array3.shift();
	console.log(array3);
	console.log(removedItemShift1);

	array3.pop();
	console.log(array3); 

	array3.unshift('George');
	console.log(array3);

	array3.push('Michael');
	console.log(array3);


	// Solution from Ms. Thonie

	array3.shift();
	console.log(array3);
	array3.pop();
	console.log(array3);
	array3.unshift('George');
	console.log(array3);
	array3.push('Michael');
	console.log(array3);

	// .sort() - by default, will allow us to sort out items in ascending order.

	array3.sort();
	console.log(array3);

	let numArray1 = [3, 2, 1, 6, 7, 9];
	numArray1.sort();
	console.log(numArray1);

	let numArray3 = [32, 400, 450, 450, 50, 9, 2,];
	numArray3.sort((a,b) => a-b);
	console.log(numArray3); //converted to string, alphabetically sorted 

// ascending sort per number's value

numArray3.sort(function(a,b) {
	return a-b
})
console.log(numArray3);

// descending sort

numArray3.sort (function(a,b){
	return b-a
})
console.log(numArray3);


let arrayStr = ['Marie', 'Zen', "Jamie", 'Elaine'];
arrayStr.sort(function(a,b){
	return b-a
})
console.log(arrayStr);

// .reverse() - reversed the order of the items
arrayStr.sort();
console.log(arrayStr);

arrayStr.sort().reverse();
console.log(arrayStr);

let beatles = ['George', 'John','Paul', 'Ringo'];
let lakersPlayers = ['Lebron', 'Davis', 'Westbrook', 'Kobe', 'Shaq'];

// Splice() - allows us toe remove and add elements from a given index. 
// Syntax: array.splicve(startingIndex, numberOfItemstobeDeleted, elementstoAdd)

lakersPlayers.splice(0, 0, 'Caruso');
console.log(lakersPlayers);

lakersPlayers.splice(0, 1,);
console.log(lakersPlayers);

lakersPlayers.splice(0, 3,);
console.log(lakersPlayers);

lakersPlayers.splice(1, 1,);
console.log(lakersPlayers);

lakersPlayers.splice(1, 0, 'Gasol', 'Fisher');
console.log(lakersPlayers);

// Non-mutators - methods that will not change the original array
// slice() - allows us to get a portion of the original array and return a new array with the items selected frrom the original
// syntax: slice(startIndex, endIndex)

let computerBrands = ['IBM', 'HP', 'apple', 'MSI'];
computerBrands.splice(2,2,'Compaq', 'Toshiba', 'Acer')
console.log(computerBrands);

let newBrands = computerBrands.slice(1,3);
console.log(computerBrands);
console.log(newBrands);

let fonts =['Verdana', 'Calibri', 'Impact', 'Windings', 'Comoc San MS', 'Arial'];
console.log(fonts);


let newFontSet = fonts.slice(1,4);
console.log(newFontSet);

newFontSet = fonts.slice(); //i re-return nya lahat ng elements
console.log(newFontSet);

/*
	Mini-activity

	Given a set of data, use slice to copy the last two items in the array.

	Save the sliced portion of the array into a new variable:

		-microsoft
	Use slice to copy the third and fourth items in the array.
	Save the sliced portion of the array into a new variable:
		-nintendo
	Log both new arrays in the console.

*/
let videoGame = ['PS4', 'PS5', 'Switch', 'Xbox', 'Xbox1'];
console.log(videoGame);

let microsoft = videoGame.slice(3);
console.log(microsoft);

let nintendo = videoGame.slice(2, 4);
console.log(nintendo);

// .toString() - convert the array into a single value as a string but each item will be separated by a comma
// syntax: array.toString()

let sentence = ['I', 'like', 'Javascript', '.', 'It', 'is', 'fun', '.'];
let sentenceString = sentence.toString();
console.log(sentence);
console.log(sentenceString);

// .join() - converts the array into a single value as a string but separator can be specified

// syntax: array.join(separator);

let sentence1 = ['My', 'fave', 'fastfood', 'is', 'Army Navy'];
let sentenceString1 = sentence1.join(' ');
console.log(sentenceString1);
let sentenceString2 = sentence1.join("/");
console.log(sentenceString2);

console.log('==========October 08, 2021==========')
/*
	Mini - activity

	Given a set of characters:
		-form the name, "Martin" as single string.
		-form the name, "Miguel" as a single string.
	Use the methods we discussed so far.

	Save "Martin" and "Miguel to a variables":
		name1 and name2
	Log both variables on the console

*/

let nameMartin = ['M','A', 'R', 'T', 'I', 'N'];
let nameMiguel = ['M','I', 'G', 'U', 'E', 'L'];
console.log(array);

let name1 = nameMartin.join('');
console.log(name1);
let name2 = nameMiguel.join('');
console.log(name2);

// Solution of Ms. Thonie

let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];

let name3 = charArr.slice(5,11).join("");
let name4 = charArr.slice(13,19).join("");
console.log(name3, name4);


// .concat() - it combines 2 or more arrays without affecting the original
// syntax: array.concat(array1, array2)

let tasksFriday = ['drink HTML', 'eat Javascript'];
let tasksSaturday = ['inhale CSS', 'exhale Bootstrap'];
let tasksSunday = ['Get Git', 'Be Node'];

let weekendTasks = tasksFriday.concat(tasksSaturday,tasksSunday);
console.log(weekendTasks);

// Accessors - methods that allow us to access our array.
// indexOf() - it finds the index of the given element/item when it is first found from the left.

let batch131 = [
	'Paolo',
	'Jamir',
	'Jed',
	'Ronel',
	'Rom',
	'Jayson'
]; 

console.log(batch131); 
console.log(batch131.indexOf('Jed')); //2
console.log(batch131.indexOf('Rom')); //4

// lastIndexOf() - finds the index of the given element/item when it is last found from the right

console.log(batch131.lastIndexOf('Jamir')); //1
console.log(batch131.lastIndexOf('Jayson')); //5
console.log(batch131.lastIndexOf('')); //-1
console.log(batch131.lastIndexOf('Kim')); //-1

/*
		Mini-activity
	Given a set brands with some entries repeated:
		Create a function which can display the index of the brand that was input the first time it was found in the array.

		Create a function which can display the index of the brand that was input tha last time it was found in the array.

*/

	let carBrands = [
		'BMW',
		'Dodge',
		'Maserati',
		'Porsche',
		'Chevrolet',
		'Ferrari',
		'GMC',
		'Porsche',
		'Mitsubishi',
		'Toyota',
		'Volkswagen',
		'BMW'
	];

	function carsIndexFirst(cars) {
		console.log(carBrands.indexOf(cars));
	}

	function carsIndexLast(cars) {
		console.log(carBrands.lastIndexOf(cars));
	}

	carsIndexFirst('BMW');
	carsIndexLast('BMW');
	console.log(carBrands);

// Iterator Methods - these methods over the items in an array much like loop
// However, with our iterator methods there are also that allows to not only iterate over items but also additional instruction.

let avengers = [
		'Hulk',
		'Black Widow',
		'Hawkeye',
		'Spiderman',
		'Iron Man',
		'Captain America'
];

//forEach() - similar to for loop but it is use on arrays. It will allow us to iterate over each time in an array. And even added instruction per iteration.
// Anonymous function within forEach will be receive each and every item in an array.

avengers.forEach(function(avengers){
	console.log(avengers);
});

console.log(avengers);

let marvelHeroes = [
		'Moon Knight',
		'Jessica Jones',
		'Deadpool',
		'Cyclops'
];

marvelHeroes.forEach(function(hero){
	//iterate over all the items in Marvel Heroes array and let them join the avengers
	if(hero !=='Cyclops' && hero !=='Deadpool') {
		//Add an if-else wherein Cyclops and Deadpool allo to join
		avengers.push(hero);
	}
});
console.log(avengers);

// map() - similar to forEach however it returns new array

let number = [25, 50, 30, 10, 5];

let mappedNumbers = number.map(function(number){
	console.log(number);
	return number * 5
});

console.log(mappedNumbers);

/*
	every() - iterates overall the items and check if all the elements passes a given conditions.
*/

let allMemberAge = [ 25, 30, 15, 20, 26];

let checkAllAdult = allMemberAge.every(function(age){
	console.log(age);
	return age >=18
});

console.log(checkAllAdult);

/*
	some() - iterate overall the items check if even at least one of items in the array passes the condition. Same as every(), it will return boolean.

*/

let examScores = [75, 80, 74, 71];
let checkForPassing = examScores.some(function(score){
	console.log(score);
	return score >=80;
});
console.log(checkForPassing);

/*
	filter() - creates a new array that contains elements which passed a given condition
*/

let numbersArr2 = [500, 12, 120, 60, 6, 30];
let divisibleBy5 = numbersArr2.filter(function(number){
	return number % 5 ===0;
});

console.log(divisibleBy5);

/*
	find() - iterate overall items in our array but only returns the item that wil satisfy the given condition
*/
let registeredUsernames = ['pedro101', 'mikeyTheKing2000', 'superPhoenix', 'sheWhoCode'];
let foundUser = registeredUsernames.find(function(username){
		console.log(username);
		return username === 'mikeyTheKing2000'; 
});

console.log(foundUser);

/*
	.include() - returns a boolean true if it finds a matching item in the array.

	.includes() is a case sensitive iterator
*/

let registeredEmails = [
		'johnnyPhoenix1991@gmail.com',
		'michaelKing@gmail.com',
		'pedro_himself@yahoo.com',
		'sheJonesSmith@gmail.com'
];

let doesEmailExist = registeredEmails.includes('michaelKing@gmail.com');
console.log(doesEmailExist);

/*
	Mini - activity:

	Create 2 functions
		First Function is able to find specified or the username input in our registeredUsernames array.
		Display the result in the console.

		Second Function is able to find a specified email already exist in the registeredEmails array.
			-IF there is an email found, show an alert:
			"Email Already Exist"
			-IF there is no email found, show an alert:
				"Email is available, proceed to registration."

		You may use any of the three methods we discussed recently.
*/


let testFindUser = registeredUsernames.find(function(username){
		console.log(username);
		return username === 'mikeyTheKing2000'; 
});

console.log(testFindUser);


function takenEmailTester(email){
	let takenEmailTester = registeredEmails.includes('pedro_himself@yahoo.com');
		if (email !== true){
			alert('Email Already Exist.');
		} else {
			alert('Email is available, proceed to registration.')
		}
	}
 takenEmailTester('pedro_himself@yahoo.com');
